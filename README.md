## This is my final project for cyber
This is an app that creates paths for robots.
It is a client with gui.

## Things you can do with this
* you can create paths for your robot.
* you can save the paths that you create.
* you can load paths that you created and change them if you want.
* you can change the robot limitations in gui.

## Things you need to do before run
* when you call GUIClient.main() you need to enter a path for a picture.