import setuptools
from pip._internal import main as pip


def install_pack(package):
    pip(['install', package])


with open("README.md", "r") as fh:
    long_description = fh.read()

install_pack('pyautogui')
install_pack('casadi')
install_pack('matplotlib')
install_pack('tkinter')
install_pack('PIL')
install_pack('csv')
install_pack('keyboard')

setuptools.setup(
    name="final_project",  # Replace with your own username
    version="1.0",
    author="Eran Ovadia",
    author_email="eran.ovadia8@gmail.com",
    description="A small example package",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/EranOvadia/final-project",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',
)
