import sqlite3


class DataBase(object):
    filename = None
    connection = None
    cur = None

    def __init__(self, filename=None):
        self.filename = filename

    def connect(self):
        self.connection = sqlite3.connect('{}.db'.format(self.filename))
        self.cur = self.connection.cursor()

    def close(self):
        self.connection.close()

    def commit(self):
        self.connection.commit()

    def create_table_points(self):
        self.connection.execute(
            'CREATE TABLE IF NOT EXISTS points (type    TEXT PRIMARY KEY    NOT NULL, x    FLOAT    NOT NULL, y    FLOAT    NOT NULL, heading    FLOAT    NOT NULL, consider    BOOLEAN    NOT NULL, vel    FLOAT, theta    FLOAT, rotation FLOAT);')

    def create_table_maxes(self):
        self.connection.execute(
            'CREATE TABLE IF NOT EXISTS maxes (type    TEXT PRIMARY KEY    NOT NULL, size    FLOAT    NOT NULL);')

    def add_values_points(self, values):
        for value in values:
            self.cur.execute(
                'INSERT INTO points (type, x, y, heading, consider, vel, theta, rotation) VALUES (\'{}\', \'{}\', \'{}\', \'{}\', \'{}\', \'{}\', \'{}\', \'{}\')'.format(
                    value[0], value[1], value[2], value[3], value[4], value[5], value[6], value[7]))
            self.commit()

    def add_values_maxes(self, values):
        for value in values:
            self.cur.execute(
                'INSERT INTO maxes (type, size) VALUES (\'{}\', \'{}\')'.format(value[0], value[1]))
            self.commit()

    def drop_tables_points_and_maxes(self):
        self.drop_table('points')
        self.drop_table('maxes')

    def drop_table(self, table_name):
        self.cur.execute("DROP TABLE {}".format(table_name))

    def get_values_table(self, table_name):
        data = []
        self.cur.execute('SELECT * FROM {}'.format(table_name))
        for row in self.cur.fetchall():
            row_value = []
            for value in row:
                row_value.append(value)
            data.append(row_value)
        return data


if __name__ == '__main__':
    start = ('start_point', 0, 0, 0, True)
    max_v = ('max_v', 3)
    db = DataBase('try')
    db.connect()
    db.create_table_points()
    db.create_table_maxes()
    db.add_values_points([start])
    db.add_values_maxes([max_v])
    print(db.get_values_table('points'))
    print(db.get_values_table('maxes'))
    db.close()
