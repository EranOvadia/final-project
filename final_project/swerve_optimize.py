from casadi import *
import matplotlib.pyplot as plt

# import numpy as np

# you can find documentation on casadi here:
# https://web.casadi.org/docs/
# https://github.com/casadi/casadi/tree/master/docs/examples/python
# https://web.casadi.org/blog/ocp/
# https://web.casadi.org/blog/opti/
# https://www.youtube.com/watch?v=JI-AyLv68Xs
# https://github.com/MMehrez/MPC-and-MHE-implementation-in-MATLAB-using-Casadi

# opti = Opti()
# N = 100
# dt = 0.05  # only for heuristics
# x_dim = 3
# u_dim = 3
#
# max_v = 3
# max_theta_dot = pi / 1
# max_v_dot = 1.5
# max_omega = pi / 1
# max_omega_dot = 3
#
# L = 0.6  # width of the drive train
# x_epsilon = 0.0001
# u_epsilon = 0.001
#
# Q = DM([[3., 0, 0],
#         [0, 3., 0],
#         [0, 0, 1]])
#
# r = 0.01
# R = DM([[r, 0, 0],
#         [0, 0, 0],
#         [0, 0, r]])

# x_init = DM([0.,
#              0.,
#              pi])

# u_init = DM([0,
#              0,
#              0])

# x_target = DM([5,
#                5.9,
#                np.deg2rad(27)])

# u_target = DM([0,
#                0.,
#                0])

waypoint1 = DM([-0.5,
                3.5,
                pi / 2])

waypoint2 = DM([3,
                4,
                0])


# waypoints = [waypoint1, waypoint2]
# waypoints = [DM([2, 2, 0])]

# segs_num = len(waypoints) + 1


class SwerveOptimize(object):
    opti = Opti()
    N = 240
    dt = 0.05  # only for heuristics
    x_dim = 3
    u_dim = 3

    # max_v = 3
    max_v = None
    # max_theta_dot = pi / 1
    max_theta_dot = None
    # max_v_dot = 1.5
    max_v_dot = None
    # max_omega = pi / 1
    max_omega = None
    # max_omega_dot = 3
    max_omega_dot = None

    # L = 0.6  # width of the drive train
    x_epsilon = 0.0001
    u_epsilon = 0.001

    # Q = DM([[3., 0, 0],
    #         [0, 3., 0],
    #         [0, 0, 1]])

    x_init = None
    u_init = None

    x_target = None
    u_target = None
    u_target_consider = None

    waypoints = None

    segs_num = None

    def __init__(self, max_v, max_a, max_theta_dot, max_omega, max_omega_dot, x_init, x_target, u_target, waypoints):
        self.max_v = max_v
        self.max_v_dot = max_a
        self.max_theta_dot = max_theta_dot
        self.max_omega = max_omega
        self.max_omega_dot = max_omega_dot
        self.x_init = x_init
        self.u_init = DM([0, 0, 0])

        self.x_target = x_target
        # self.u_target = DM([0, 0, 0])
        self.u_target = u_target[1]
        self.u_target_consider = u_target[0]

        # self.waypoints = []
        # for waypoint in waypoints:
        #     self.waypoints.append(DM([waypoint.x, waypoint.y, waypoint.heading]))
        self.waypoints = waypoints

        self.segs_num = len(self.waypoints) + 1

    def get_swerve_drive_model(self):
        x = MX.sym('x', self.x_dim)
        u = MX.sym('u', self.u_dim)

        v = u[0]
        theta = u[1]
        w = u[2]

        x_dot = vertcat(v * cos(theta),
                        v * sin(theta),
                        w)

        f = Function('f', [x, u], [x_dot])
        return f

    def set_constraints(self, opti, X, U):
        opti.subject_to(X[:, 0] == self.x_init)  # initial state constraint
        opti.subject_to(U[0, 0] == self.u_init[0])  # initial u constraint
        opti.subject_to(U[2, 0] == self.u_init[2])  # initial u constraint

        # we use a small epsilon to set the final constraint
        # because we cannot force the optimizer to reach the exact final position we want
        opti.subject_to(
            opti.bounded(-self.x_epsilon, X[:, self.N - 1] - self.x_target, self.x_epsilon))  # terminal x constraint
        opti.subject_to(
            opti.bounded(-self.u_epsilon, U[0, self.N - 1] - self.u_target[0], self.u_epsilon))  # terminal u constraint
        if self.u_target_consider:
            opti.subject_to(
                opti.bounded(-self.u_epsilon, U[1, self.N - 1] - self.u_target[1],
                             self.u_epsilon))  # terminal u constraint
        opti.subject_to(
            opti.bounded(-self.u_epsilon, U[2, self.N - 1] - self.u_target[2], self.u_epsilon))  # terminal u constraint

        if self.segs_num > 1:
            for i in range(len(self.waypoints)):
                if not self.waypoints[i][1] is False:
                    opti.subject_to(
                        opti.bounded(-self.x_epsilon,
                                     X[:, (self.N // self.segs_num) * (i + 1)] - self.waypoints[i][0][:],
                                     self.x_epsilon))  # waypoint x constraint
                else:
                    opti.subject_to(
                        opti.bounded(-self.x_epsilon,
                                     X[:2, (self.N // self.segs_num) * (i + 1)] - self.waypoints[i][0][:2],
                                     self.x_epsilon))
                if not self.waypoints[i][2] is False:
                    opti.subject_to(
                        opti.bounded(-self.u_epsilon, U[0, (self.N // self.segs_num) * (i + 1)] - self.waypoints[i][2],
                                     self.u_epsilon))
                if not self.waypoints[i][3] is False:
                    opti.subject_to(opti.bounded(-self.u_epsilon,
                                                 U[1, (self.N // self.segs_num) * (i + 1)] - self.waypoints[i][3],
                                                 self.u_epsilon))
                if not self.waypoints[i][4] is False:
                    opti.subject_to(
                        opti.bounded(-self.u_epsilon, U[2, (self.N // self.segs_num) * (i + 1)] - self.waypoints[i][4],
                                     self.u_epsilon))

    def set_dynamics_constraints(self, opti, X, U, f, T):
        # limit the trajectory to obey the model dynamics
        seg = self.N // self.segs_num
        for i in range(self.segs_num):
            dt = T[i] / seg
            for j in range(i * seg, ((i + 1) * seg)):
                if j == self.N - 1:
                    break
                dx = dt * f(X[:, j], U[:, j])
                next_state = X[:, j] + dx
                opti.subject_to(X[:, j + 1] == next_state)

    def set_input_constraints(self, opti, U, T):
        last_seg_dt = self.dt
        seg = self.N // self.segs_num
        for j in range(self.segs_num):
            dt = T[j] / seg
            for i in range(j * seg, (j + 1) * seg):
                if i == self.N - 1:
                    break

                # limit acceleration
                # v_dot = (U[0, i + 1] - U[0, i - 1]) / (self.dt * 2)
                v_dot = (U[0, i + 1] - U[0, i]) / dt
                opti.subject_to(opti.bounded(-self.max_v_dot, v_dot, self.max_v_dot))

                # limit the change of v direction (theta)
                # theta_dot = (U[1, i + 1] - U[1, i - 1]) / (self.dt * 2)
                theta_dot = (U[1, i + 1] - U[1, i]) / dt
                the = self.max_theta_dot
                # the = (self.max_v_dot - v_dot) / U[0, i]
                # the = fmin(the, self.max_theta_dot)
                # the = (self.max_v - U[0, i + 1]) * pi / (2 * self.max_v) / self.dt
                # the = (self.max_v - U[0, i + 1]) * pi / (2 * self.max_v)
                # the = -4 * pi / self.max_v + 4 * pi
                # the = -3.177 * MX.log(U[0, i + 1]) + 5.2693
                # the = 0.7096 * U[0, i + 1]**-1.233
                # max_theta_dot = pi
                # min_max_theta_dot = pi / (2 * self.max_v / s
                # self.max_v_dot)
                # the = (min_max_theta_dot - max_theta_dot) / self.max_v * U[0, i + 1] + max_theta_dot
                # the = self.max_theta_dot
                # the = 2 * pi
                opti.subject_to(opti.bounded(-the, theta_dot, the))
                # if i == 1:
                #     opti.subject_to(U[1, 1] == (theta_dot * dt) + U[1, j * seg])
                # opti.subject_to(opti.bounded(-self.max_theta_dot, theta_dot, self.max_theta_dot))

                # limit the change in omega
                # omega_dot = (U[2, i + 1] - U[2, i - 1]) / (self.dt * 2)
                omega_dot = (U[2, i + 1] - U[2, i]) / dt
                opti.subject_to((opti.bounded(-self.max_omega_dot, omega_dot, self.max_omega_dot)))

        # limit omega
        opti.subject_to(opti.bounded(-self.max_omega, U[2, :], self.max_omega))

        # wheel velocity limit
        v = U[0, :]
        theta = U[1, :]
        w = U[2, :]

        # wheel_v_1 = v ** 2 + (v * w * self.L * (-sin(theta) + cos(theta))) + ((w ** 2 * self.L ** 2) / 2)
        # wheel_v_2 = v ** 2 + (v * w * self.L * (sin(theta) + cos(theta))) + ((w ** 2 * self.L ** 2) / 2)
        # wheel_v_3 = v ** 2 + (v * w * self.L * (sin(theta) - cos(theta))) + ((w ** 2 * self.L ** 2) / 2)
        # wheel_v_4 = v ** 2 + (v * w * self.L * (-sin(theta) - cos(theta))) + ((w ** 2 * self.L ** 2) / 2)
        # opti.subject_to(opti.bounded(0, wheel_v_1, self.max_v ** 2))
        # opti.subject_to(opti.bounded(0, wheel_v_2, self.max_v ** 2))
        # opti.subject_to(opti.bounded(0, wheel_v_3, self.max_v ** 2))
        # opti.subject_to(opti.bounded(0, wheel_v_4, self.max_v ** 2))

        # limit to only positive v
        opti.subject_to(opti.bounded(0, v, self.max_v))
        # opti.subject_to(opti.bounded(-self.max_v, v, self.max_v))

    def linear_interpolation(self, x, u, t):
        # print(np.diff(t))
        new_t = np.arange(0, t[-1], 0.01)
        xs = np.array(x[:, 0])
        ys = np.array(x[:, 1])
        headings = np.array(x[:, 2])
        vs = np.array(u[:, 0])
        thetas = np.array(u[:, 1])
        ws = np.array(u[:, 2])
        new_x = np.interp(new_t, t, xs)
        new_y = np.interp(new_t, t, ys)
        new_heading = np.interp(new_t, t, headings)
        new_v = np.interp(new_t, t, vs)
        new_theta = np.interp(new_t, t, thetas)
        new_w = np.interp(new_t, t, ws)
        # plt.plot(new_x, new_y)
        # plt.show()
        return new_x, new_y, new_heading, new_v, new_theta, new_t, new_w

    def calculate(self):
        X = self.opti.variable(self.x_dim, self.N)  # system state
        U = self.opti.variable(self.u_dim, self.N)  # system inputs
        T = []  # list of times from each waypoint to the next
        for i in range(self.segs_num):
            T.append(self.opti.variable())

        f = self.get_swerve_drive_model()

        J = 0  # initialize the cost function

        # for i in range(self.N):
        #     # penalize large inputs
        #     J += U[:, i].T @ self.R @ U[:, i]
        #
        # J /= (self.N * 2.5)

        # minimize the total time of the trajectory
        for t in T:
            J += t

        self.opti.minimize(J)

        self.set_constraints(self.opti, X, U)

        self.set_dynamics_constraints(self.opti, X, U, f, T)

        self.set_input_constraints(self.opti, U, T)

        for t in T:
            # limit the time,
            self.opti.subject_to(self.opti.bounded(0.1, t, 20))
            # but guessing an initial T did improve the performance
            self.opti.set_initial(t, 0.05 * self.N / self.segs_num)

        # finally, solve the NLP
        self.opti.solver("ipopt")
        sol = self.opti.solve()

        sol_X = sol.value(X).T
        sol_U = sol.value(U).T

        seg = self.N // self.segs_num
        # calculating time list
        # time_list = np.arange(0, sol.value(T[0]), sol.value(T[0]) / seg)
        # accumulated_time = sol.value(T[0])
        #
        # for i in range(1, len(T)):
        #     a = np.arange(accumulated_time, sol.value(T[i]) + accumulated_time, sol.value(T[i]) / seg)
        #     time_list = np.concatenate([time_list, a])
        #     accumulated_time += sol.value(T[i])
        # # time_list = np.append(time_list, accumulated_time)
        time_list = [i * sol.value(T[0]) / seg for i in range(seg)]
        accumulated_time = sol.value(T[0])
        if len(T) > 1:
            for i in range(1, len(T)):
                a = [accumulated_time + (j * sol.value(T[i]) / seg) for j in range(seg)]
                time_list += a
                accumulated_time += sol.value(T[i])

        if len(time_list) == self.N + 1:
            time_list = time_list[:-1]

        # print('dt fisrt = ', time_list[seg - 1] - time_list[seg - 2])
        # print('dt before = ', time_list[seg] - time_list[seg - 1])
        # print('dt after = ', time_list[seg + 1] - time_list[seg])

        x_sol = []
        y_sol = []
        for x in sol_X:
            x_sol.append(x[0])
            y_sol.append(x[1])

        time_list = np.array(time_list)

        heading_waypoints = []
        for waypoint in range(seg, len(sol_X), seg):
            heading_waypoints.append(sol_X[waypoint][2])

        # linear interpolation to trajectory for dt = 0.01
        x, y, heading, v, theta, t, w = self.linear_interpolation(sol_X, sol_U, time_list)
        return x, y, heading, v, theta, t, w, heading_waypoints

    def main(self):
        # X = self.opti.variable(self.x_dim, self.N)  # system state
        # U = self.opti.variable(self.u_dim, self.N)  # system inputs
        # T = []  # list of times from each waypoint to the next
        # for i in range(self.segs_num):
        #     T.append(self.opti.variable())
        #
        # f = self.get_swerve_drive_model()
        #
        # J = 0  # initialize the cost function
        #
        # for i in range(self.N):
        #     # penalize large inputs
        #     J += U[:, i].T @ self.R @ U[:, i]
        #
        # # minimize the total time of the trajectory
        # for t in T:
        #     J += t
        #
        # self.opti.minimize(J)
        #
        # self.set_constraints(self.opti, X, U)
        #
        # self.set_dynamics_constraints(self.opti, X, U, f, T)
        #
        # self.set_input_constraints(self.opti, U)
        #
        # for t in T:
        #     # limit the time,
        #     self.opti.subject_to(self.opti.bounded(0.1, t, 20))
        #     # but guessing an initial T did improve the performance
        #     self.opti.set_initial(t, 0.05 * self.N / self.segs_num)
        #
        # # finally, solve the NLP
        # self.opti.solver("ipopt")
        # sol = self.opti.solve()
        #
        # sol_X = sol.value(X).T
        # sol_U = sol.value(U).T
        #
        # seg = self.N // self.segs_num
        # # calculating time list
        # time_list = np.arange(0, sol.value(T[0]), sol.value(T[0]) / seg)
        # accumulated_time = sol.value(T[0])
        #
        # for i in range(1, len(T)):
        #     a = np.arange(accumulated_time, sol.value(T[i]) + accumulated_time, sol.value(T[i]) / seg)
        #     time_list = np.concatenate([time_list, a])
        #     accumulated_time += sol.value(T[i])
        # # time_list = np.append(time_list, accumulated_time)
        #
        # # linear interpolation to trajectory for dt = 0.01
        # x, y, heading, v, theta, t, w = self.linear_interpolation(sol_X, sol_U, time_list)
        x, y, heading, v, theta, t, w, heading_waypoints = self.calculate()

        waypoints_x = [i[0][0] for i in self.waypoints]
        waypoints_y = [i[0][1] for i in self.waypoints]

        waypoints_x = [self.x_init[0]] + waypoints_x + [self.x_target[0]]
        waypoints_y = [self.x_init[1]] + waypoints_y + [self.x_target[1]]

        # plot the trajectory path
        plt.subplot(221)
        # plt.plot(sol_X[:, 0], sol_X[:, 1])
        plt.plot(x, y)
        plt.plot(waypoints_x, waypoints_y, 'ro', ms=3)
        plt.title("X-Y")
        plt.axis("equal")

        # plot the angle
        # plt.subplot(222)
        # plt.plot(sol.value(X[2, :]))
        # plt.plot(t, heading)
        # plt.title("heading")

        # plot V
        plt.subplot(223)
        # plt.plot(sol_U[:, 0])
        plt.plot(t, v)
        plt.title("v")

        # plot theta and omega
        plt.subplot(224)
        # plt.plot(sol_U[:, 1])
        # plt.plot(sol_U[:, 2])
        plt.plot(t, theta, '.')
        plt.plot(t, w, '.')
        plt.title("theta_dot and omega")

        N = self.N

        # print(f"Average dt: {sum([sol.value(t) for t in T]) / N}")
        # print(f"Total time: {sum([sol.value(t) for t in T])}")
        plt.show()


if __name__ == '__main__':
    # o = Optimize(DM([0, 0, 0]), DM([5, 5, 0]), [waypoint1, waypoint2])
    # def __init__(self, max_v, max_a, max_theta_dot, max_omega, max_omega_dot, x_init, x_target, u_target, waypoints):
    # o = SwerveOptimize(3, 3, 3, 3, 3, DM([0, 0, 0]), DM([5, 6, 0]), [False, DM([0, 0, 0])],
    #                    [[waypoint1, True, False, DM([3, 0, 0])], [waypoint2, True, False, DM([3, 0, 0])]])
    o = SwerveOptimize(3, 3, 3, 3, 3, DM([0, 0, 0]), DM([6, 0, 0]), [False, DM([0, 0, 0])],
                       [[DM([3, 0, 0]), True, True, DM([1, 0, 0])]])
    o.main()
