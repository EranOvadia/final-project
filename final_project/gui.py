import csv
import math
import pickle
import threading
import time
from tkinter import *
import tkinter as tk

import keyboard as keyboard
from PIL import ImageTk, Image, ImageGrab

from final_project.Constants import *
from final_project.swerve_optimize import *

# width_picture = 1393
width_picture = 1377
height_picture = 700
# pixel_to_meter = float(height_picture) / 8.23
# pixel_to_meter = None

width_drive_system = 0.74
height_drive_system = 0.74

# deploy_csv = 'C:\\Users\\3075\\VisualStudioProjects\\Swerve2020\\swerve2020\\SwrveDrive\\src\\main\\deploy\\'
# deploy_csv = 'C:\\VisualStudioProjects\\Swerve2020\\swerve2020\\SwrveDrive\\src\\main\\deploy\\'
# deploy_csv = 'C:\\VisualStudioProjects\\InfiniteRecharge2020\\infinite-recharge-2020\\infinite-recharge-2020\\src\\main\\deploy\\'
deploy_csv = 'C:\\Users\\User\\VisualStudioProjects\\InfiniteRecharge2020\\infinite-recharge-2020\\infinite-recharge-2020\\src\\main\\deploy\\'


class Point(object):
    root = None  # type -> tk
    canvas = None  # type -> tk.canvas

    x_pos = None  # type -> float
    y_pos = None  # type -> float
    heading_pos = None  # type -> float
    oval = None  # type -> id of oval from tk.canvas
    rect = None  # type -> id of polygon from tk.canvas
    arrow = None  # type -> id of line from tk.canvas
    color = None  # type -> string

    name = None  # type -> string
    x = None  # type -> int
    y = None  # type -> int

    width_pic = None  # type -> int
    height_pic = None  # type -> int

    size_point = 6
    orgi_size_point = 6
    size_rect_width = width_drive_system * pixel_to_meter / 2.0
    size_rect_height = height_drive_system * pixel_to_meter / 2.0
    size_arrow = width_drive_system * pixel_to_meter / 2.0

    name_message = None  # type -> tk.message
    x_message = None  # type -> tk.message
    y_message = None  # type -> tk.message
    heading_message = None  # type -> tk.message
    heading_consider_message = None  # type -> tk.message

    x_text = None  # type -> tk.text
    y_text = None  # type -> tk.text
    heading_text = None  # type -> tk.text

    # submit_button = None  # type -> tk.button

    heading_consider_var = None
    heading_consider_option_menu = None
    heading_consider_options = ['Enable', 'Disable']

    color_message = None

    v_text = None
    theta_text = None
    rotation_text = None
    v_consider_var = None
    v_consider_option_menu = None
    v_consider_options = ['Enable', 'Disable']

    vel = 0
    theta = 0
    rotation = 0

    pressed = False

    def __init__(self, root, canvas, name, x, y, color, width_pic, height_pic):
        self.root = root
        self.canvas = canvas
        self.name = name
        self.x = x
        self.y = y
        self.color = color
        self.width_pic = width_pic
        self.height_pic = height_pic

        # self.init_messages()
        self.init_texts()
        # self.init_buttons()
        self.init_option_menu()
        self.place_all()

    def init_messages(self):
        self.name_message = Message(master=self.root, text=self.name, width=60)

        self.x_message = Message(master=self.root, text='x:')

        self.y_message = Message(master=self.root, text='y:')

        self.heading_message = Message(master=self.root, text='heading:', width=60)

        self.heading_consider_message = Message(master=self.root, text='consider heading:', width=100)

    def init_texts(self):
        self.x_text = Text(master=self.root, width=7, height=1)

        self.y_text = Text(master=self.root, width=7, height=1)

        self.heading_text = Text(master=self.root, width=7, height=1)

        self.color_message = Message(master=self.root, text='', background=self.color)

        # self.v_text = Text(master=self.root, width=7, height=1)
        # self.theta_text = Text(master=self.root, width=7, height=1)
        # self.rotation_text = Text(master=self.root, width=7, height=1)

        text_list = self.x_text, self.y_text, self.heading_text
        for i, text in enumerate(text_list):
            text.bind('<Tab>', lambda e, text=text, num=i + 1: self.focus_next(text, num))
            text.bind('<Shift-Tab>', lambda e, text=text, num=i + 1: self.focus_prev(text, num))
            text.bind('<Return>', lambda e: self.inputs(True))

    @staticmethod
    def focus_next(text, num):
        if num % 3 == 0:
            text.tk_focusNext().tk_focusNext().focus_set()
        else:
            text.tk_focusNext().focus_set()
        return 'break'

    @staticmethod
    def focus_prev(text, num):
        if num == 1:
            text.tk_focusPrev().tk_focusPrev().focus_set()
        else:
            text.tk_focusPrev().focus_set()
        return 'break'

    # def init_buttons(self):
    #     self.submit_button = Button(master=self.root, text='submit', command=self.inputs)

    def init_option_menu(self):
        self.heading_consider_var = StringVar(master=self.root)
        if self.name == 'start point' or self.name == 'end point':
            self.heading_consider_var.set(self.heading_consider_options[0])
        else:
            self.heading_consider_var.set(self.heading_consider_options[1])
            self.v_consider_var = StringVar(master=self.root)
            self.v_consider_var.set(self.v_consider_options[1])
            self.v_consider_option_menu = OptionMenu(self.root, self.v_consider_var, *self.heading_consider_options)
        self.heading_consider_option_menu = OptionMenu(self.root, self.heading_consider_var,
                                                       *self.heading_consider_options)

    def place_all(self):
        # self.name_message.place(x=self.x, y=self.y)
        # self.x_message.place(x=self.x + 100 * screen_width, y=self.y)
        # self.y_message.place(x=self.x + 200 * screen_width, y=self.y)
        # self.heading_message.place(x=self.x + 300 * screen_width, y=self.y)
        # self.heading_consider_message.place(x=self.x + 500 * screen_width, y=self.y)

        self.x_text.place(x=self.x, y=self.y)
        self.y_text.place(x=2 * self.x, y=self.y)
        self.heading_text.place(x=3 * self.x, y=self.y)

        # self.submit_button.place(x=self.x + 425 * screen_width, y=self.y)

        # self.heading_consider_option_menu.place(x=4 * self.x, y=self.y)
        # if not self.v_consider_option_menu is None:
        #     self.v_consider_option_menu.place(x=5.5 * self.x, y=self.y)

        self.color_message.place(x=self.x - 50 * screen_width, y=self.y)

        if not self.name == 'start point' and not self.name == 'end point':
            self.place_vel()

    def __call__(self, *args, **kwargs):
        heading = ''
        if self.state_heading():
            heading = self.heading_pos
        return self.x_pos, self.y_pos, heading, self.vel, self.theta, self.rotation

    def state_heading(self):
        return self.heading_consider_var.get() == 'Enable'

    def set_state_heading(self, consider):
        if consider:
            self.heading_consider_var.set('Enable')
        else:
            self.heading_consider_var.set('Disable')

    def state_vel(self):
        return self.v_consider_var.get() == 'Enable'

    def set_state_vel(self, consider):
        if consider:
            self.v_consider_var.set('Enable')
        else:
            self.v_consider_var.set('Disable')

    def update_vels(self):
        try:
            data = self.v_text.get('1.0', 'end-1c')
            if data == '':
                self.vel = False
            else:
                self.vel = float(self.v_text.get('1.0', 'end-1c'))
            data = self.theta_text.get('1.0', 'end-1c')
            if data == '':
                self.theta = False
            else:
                self.theta = math.radians(float(self.theta_text.get('1.0', 'end-1c')))
            data = self.rotation_text.get('1.0', 'end-1c')
            if data == '':
                self.rotation = False
            else:
                self.rotation = math.radians(float(self.theta_text.get('1.0', 'end-1c')))
        except Exception as e:
            print(e)

    def place_vel(self):
        if self.v_text is None:
            self.v_text = Text(master=self.root, width=7, height=1)
            self.theta_text = Text(master=self.root, width=7, height=1)
            self.rotation_text = Text(master=self.root, width=7, height=1)
        self.v_text.place(x=4 * self.x, y=self.y)
        self.theta_text.place(x=5 * self.x, y=self.y)
        self.rotation_text.place(x=6 * self.x, y=self.y)

    def destroy_vel(self):
        if not self.v_text is None:
            self.v_text.destroy()
            self.theta_text.destroy()
            self.rotation_text.destroy()
            self.v_text = None
            self.theta_text = None
            self.rotation_text = None

    def inputs(self, is_backspace):
        if is_backspace:
            keyboard.press_and_release('backspace')
        self.x_pos = float(self.x_text.get("1.0", 'end-1c'))
        self.y_pos = float(self.y_text.get("1.0", 'end-1c'))
        data = self.heading_text.get('1.0', 'end-1c')
        if data == '':
            # self.heading_pos = 0
            self.heading_consider_var.set(self.heading_consider_options[1])
        else:
            self.heading_consider_var.set(self.heading_consider_options[0])
            self.heading_pos = float(self.heading_text.get("1.0", 'end-1c'))
        self.draw_point()

    def change_pos(self, x, y, heading):
        self.x_pos = x / pixel_to_meter
        self.y_pos = (self.height_pic - y) / pixel_to_meter
        self.heading_pos = heading

        self.x_text.delete('1.0', END)
        self.x_text.insert(END, self.x_pos)
        self.y_text.delete('1.0', END)
        self.y_text.insert(END, self.y_pos)
        if self.state_heading():
            self.heading_text.delete('1.0', END)
            self.heading_text.insert(END, self.heading_pos)

        self.draw_point()

    # def set_sizes(self):
    #     self.size_rect_width = width_drive_system * pixel_to_meter / 2
    #     self.size_rect_height = height_drive_system * pixel_to_meter / 2
    #     self.size_arrow = width_drive_system * pixel_to_meter / 2

    def draw_point(self):
        # self.set_sizes()
        x_oval = self.x_pos * pixel_to_meter
        y_oval = self.height_pic - self.y_pos * pixel_to_meter
        if self.oval:
            self.canvas.delete(self.oval)
        if self.rect:
            self.canvas.delete(self.rect)
        if self.arrow:
            self.canvas.delete(self.arrow)
        self.oval = self.canvas.create_oval(x_oval - self.size_point, y_oval - self.size_point,
                                            x_oval + self.size_point, y_oval + self.size_point,
                                            fill=self.color)
        x_rect = x_oval
        y_rect = y_oval
        points = [[x_rect - self.size_rect_width, y_rect - self.size_rect_height],
                  [x_rect + self.size_rect_width, y_rect - self.size_rect_height],
                  [x_rect + self.size_rect_width, y_rect + self.size_rect_height],
                  [x_rect - self.size_rect_width, y_rect + self.size_rect_height]]
        angle = math.radians(self.heading_pos)
        cos_val = math.cos(angle)
        sin_val = math.sin(-angle)
        cx, cy = x_rect, y_rect
        new_points = []
        for x_old, y_old in points:
            x_old -= cx
            y_old -= cy
            x_new = x_old * cos_val - y_old * sin_val
            y_new = x_old * sin_val + y_old * cos_val
            new_points.append([x_new + cx, y_new + cy])
        # return new_points
        self.rect = self.canvas.create_polygon(new_points, outline=self.color, fill='')

        self.arrow = self.canvas.create_line(x_oval, y_oval, x_oval + cos_val * self.size_arrow,
                                             y_oval + sin_val * self.size_arrow,
                                             fill=self.color, width=10.0 * screen_width, arrow=tk.LAST)

    def destroy(self):
        try:
            self.x_text.destroy()
            self.y_text.destroy()
            self.heading_text.destroy()
        except Exception as e:
            print(e)

        self.canvas.delete(self.oval)
        self.canvas.delete(self.rect)
        self.canvas.delete(self.arrow)

        self.color_message.destroy()

        # self.submit_button.destroy()

        # self.name_message.destroy()
        # self.x_message.destroy()
        # self.y_message.destroy()
        # self.heading_message.destroy()
        # self.heading_consider_message.destroy()

        if not self.name == 'start point' and not self.name == 'end point':
            self.destroy_vel()

        self.heading_consider_option_menu.destroy()


class GUI(object):
    root = None
    canvas = None
    # pic = None

    place_x = None
    place_y = None
    place_bars = None

    path_pic = None

    width_pic = None
    height_pic = None

    ovals = None
    size_point = 2

    filename = None

    optimizer = None
    start_point = None
    end_point = None
    waypoints = None
    max_v = None
    max_a = None
    max_theta_dot = None
    max_omega = None
    max_omega_dot = None

    colors = ["blue", "orange", "cyan", "brown", "magenta", "gold", "silver", "turquoise"]

    driveSystem_thread = None
    driveSystem_var = None
    driveSystem_option_menu = None
    driveSystem_options = ['Swerve', 'Tank']

    add_button = None
    remove_button = None
    calculate_button = None
    trajectory_button = None

    follow_thread = None

    cool_path_finder = None

    save_text = None
    save_button = None
    save_enable = False

    finish = False

    load_thread = None
    load_var = None
    load_option_menu = None
    load_curves = ['load']
    last_load_curve = 'load'

    img_scale = 1.0
    container = None
    img = None
    orgi_img = None

    max_v_text = None
    max_v_message = None
    max_a_text = None
    max_a_message = None
    max_theta_dot_text = None
    max_theta_dot_message = None
    max_omega_text = None
    max_omega_message = None
    max_omega_dot_text = None
    max_omega_dot_message = None

    trajectory_lists = None

    x_message = None
    y_message = None
    heading_message = None
    vel_message = None
    theta_message = None
    rotation_message = None

    def __init__(self, root, path_pic, width_pic, height_pic):
        self.root = root
        self.path_pic = path_pic
        self.width_pic = width_pic
        self.height_pic = height_pic

        # start_dir = os.getcwd()
        # os.chdir(start_dir + '\\curves')
        # for name in os.listdir('.'):
        #     self.load_curves.append(name)
        # os.chdir(start_dir)

        self.ovals = []
        self.waypoints = []

        # self.place = int(800.0 * screen_height)
        self.place_x = 100 * screen_width
        self.place_y = int(750 * screen_height)
        self.place_bars = 300 * screen_height

        self.canvas = Canvas(master=self.root, width=self.width_pic, height=self.height_pic)
        self.canvas.place(x=20, y=5)

        self.orgi_img = Image.open(self.path_pic)
        self.orgi_img = self.orgi_img.resize((width_pic, height_pic), Image.ANTIALIAS)
        self.img = None
        self.img_scale = 1.0
        self.container = self.canvas.create_rectangle(0, 0, self.width_pic, self.height_pic, width=0)
        self.show_image()
        # self.canvas.create_image(int(self.width_pic / 2), int(self.height_pic / 2), image=self.pic)

        self.start_point = Point(root=self.root, canvas=self.canvas, name='start point', x=self.place_x,
                                 y=self.place_y + 50 * screen_height, color='green', width_pic=self.width_pic,
                                 height_pic=self.height_pic)
        self.start_point.change_pos(x=7 * pixel_to_meter, y=self.height_pic - 4 * pixel_to_meter, heading=0)
        self.end_point = Point(root=self.root, canvas=self.canvas, name='end point', x=self.place_x,
                               y=self.place_y + 100 * screen_height, color='red', width_pic=self.width_pic,
                               height_pic=self.height_pic)
        self.end_point.change_pos(x=9 * pixel_to_meter, y=self.height_pic - 4 * pixel_to_meter, heading=0)

        self.canvas.bind('<B1-Motion>', self.mouse_drag)
        self.canvas.bind('<ButtonRelease-1>', self.mouse_release)

        self.init_window()
        self.draw_scales()

        self.follow_thread = threading.Thread(target=self.run_follow_thread)
        self.follow_thread.start()

    def init_window(self):
        self.x_message = Message(master=self.root, text='x:')
        self.x_message.place(x=self.place_x, y=self.place_y)
        self.y_message = Message(master=self.root, text='y:')
        self.y_message.place(x=2 * self.place_x, y=self.place_y)
        self.heading_message = Message(master=self.root, text='head:')
        self.heading_message.place(x=3 * self.place_x, y=self.place_y)
        self.vel_message = Message(master=self.root, text='vel: ')
        self.vel_message.place(x=4 * self.place_x, y=self.place_y)
        self.theta_message = Message(master=self.root, text='theta: ', width=40)
        self.theta_message.place(x=5 * self.place_x, y=self.place_y)
        self.rotation_message = Message(master=self.root, text='rotaion: ', width=50)
        self.rotation_message.place(x=6 * self.place_x, y=self.place_y)
        # self.consider_message = Message(master=self.root, text='consider:', width=50)
        # self.consider_message.place(x=4 * self.place_x, y=self.place_y)

        self.add_button = Button(master=self.root, text='add', width=8, command=self.add)
        # self.add_button.place(x=self.width_pic / 2 + 100 * screen_width, y=self.place_y)
        self.add_button.place(x=6 * self.place_x, y=self.place_y + 50 * screen_height)

        self.remove_button = Button(master=self.root, text='remove', width=8, command=self.remove)
        # self.remove_button.place(x=self.width_pic / 2 + 100 * screen_width, y=self.place_y + 25 * screen_height)
        # self.remove_button.place(x=self.width_pic / 2 + 100 * screen_width, y=self.place_y + 40 * screen_height)
        self.remove_button.place(x=7 * self.place_x, y=self.place_y + 50 * screen_height)

        self.calculate_button = Button(master=self.root, text='calculate', width=25, command=self.calculate)
        # self.calculate_button.place(x=self.width_pic / 2 + 140 * screen_width, y=self.place_y)
        # self.calculate_button.place(x=self.width_pic / 2 + 160 * screen_width, y=self.place_y)
        self.calculate_button.place(x=1920 * screen_width - 300 * screen_width, y=self.place_y)

        self.trajectory_button = Button(master=self.root, text='trajectory', width=15, command=self.trajectory)
        # self.trajectory_button.place(x=self.width_pic / 2 + 200 * screen_width, y=self.place_y)
        # self.trajectory_button.place(x=self.width_pic / 2 + 250 * screen_width, y=self.place_y)
        self.trajectory_button.place(x=1920 * screen_width - 250 * screen_width, y=self.place_y + 75 * screen_height)

        self.save_text = Text(master=self.root, width=7, height=1)
        # self.save_text.place(x=self.width_pic + 100 * screen_width, y=200 * screen_height)
        self.save_text.place(x=1920 * screen_width - 150 * screen_width, y=50 * screen_height)

        self.save_button = Button(master=self.root, text='save', command=self.save)
        # self.save_button.place(x=self.width_pic + 100 * screen_width, y=250 * screen_height)
        self.save_button.place(x=1920 * screen_width - 250 * screen_width, y=50 * screen_height)

        self.load_var = StringVar(master=self.root)
        self.load_var.set(self.load_curves[0])
        self.load_option_menu = OptionMenu(self.root, self.load_var, *self.load_curves)
        # self.load_option_menu.place(x=self.width_pic + 100 * screen_width, y=300 * screen_height)
        self.load_option_menu.place(x=1920 * screen_width - 200 * screen_width, y=100 * screen_height)

        self.max_v_message, self.max_v_text = self.init_max('max v', self.set_max)
        self.set_max('max v', False)
        self.max_a_message, self.max_a_text = self.init_max('max a', self.set_max)
        self.set_max('max a', False)
        self.max_theta_dot_message, self.max_theta_dot_text = self.init_max('max theta dot', self.set_max)
        self.set_max('max theta dot', False)
        self.max_omega_message, self.max_omega_text = self.init_max('max omega', self.set_max)
        self.set_max('max omega', False)
        self.max_omega_dot_message, self.max_omega_dot_text = self.init_max('max omega dot', self.set_max)
        self.set_max('max omega dot', False)

        self.cool_path_finder = Message(master=self.root, text='Path Finder', background="darkviolet", width=400)
        self.cool_path_finder.config(font=("Courier", 44))
        self.cool_path_finder.place(x=1920 * screen_width - 800 * screen_width, y=800 * screen_height)

    def set_max_v(self, is_backspace):
        if is_backspace:
            keyboard.press_and_release('backspace')
        self.max_v = float(self.max_v_text.get("1.0", 'end-1c'))

    def set_max_a(self, is_backspace):
        if is_backspace:
            keyboard.press_and_release('backspace')
        self.max_a = float(self.max_a_text.get("1.0", 'end-1c'))

    def init_max(self, name, command):
        message = Message(master=self.root, text='{}:'.format(name), width=100)
        # message.place(x=self.width_pic + 100 * screen_width, y=self.place_bars)
        message.place(x=1920 * screen_width - 300 * screen_width, y=self.place_bars)
        text = Text(master=self.root, width=7, height=1)
        # text.place(x=self.width_pic + 250 * screen_width, y=self.place_bars)
        text.place(x=1920 * screen_width - 150 * screen_width, y=self.place_bars)
        text.bind('<Return>', lambda e: command(name, True))
        if name == 'max v':
            text.insert(END, 4.2)
        elif name == 'max a':
            text.insert(END, 3)
        elif name == 'max theta dot':
            text.insert(END, 180)
        elif name == 'max omega':
            text.insert(END, 120)
        else:
            text.insert(END, 180)
        # command(False)

        self.place_bars += 50 * screen_height
        return message, text

    def set_max(self, name, is_backspace):
        if is_backspace:
            keyboard.press_and_release('backspace')

        if name == 'max v':
            self.max_v = float(self.max_v_text.get("1.0", 'end-1c'))

        if name == 'max a':
            self.max_a = float(self.max_a_text.get("1.0", 'end-1c'))

        if name == 'max theta dot':
            self.max_theta_dot = math.radians(float(self.max_theta_dot_text.get("1.0", 'end-1c')))

        if name == 'max omega':
            self.max_omega = math.radians(float(self.max_omega_text.get("1.0", 'end-1c')))

        if name == 'max omega dot':
            self.max_omega_dot = math.radians(float(self.max_omega_dot_text.get("1.0", 'end-1c')))

    def run_follow_thread(self):
        while True:
            self.run_load_thread()
            if self.finish:
                return

    def show_image(self):
        if self.img is None:
            self.img = ImageTk.PhotoImage(self.orgi_img)
            self.canvas.create_image(int(self.width_pic / 2), int(self.height_pic / 2), image=self.img)
            return
        bbox1 = self.canvas.bbox(self.container)
        bbox1 = (bbox1[0] + 1, bbox1[1] + 1, bbox1[2] - 1, bbox1[3] - 1)
        bbox2 = (self.canvas.canvasx(0),  # get visible area of the canvas
                 self.canvas.canvasy(0),
                 self.canvas.canvasx(self.canvas.winfo_width()),
                 self.canvas.canvasy(self.canvas.winfo_height()))
        bbox = [min(bbox1[0], bbox2[0]), min(bbox1[1], bbox2[1]),  # get scroll region box
                max(bbox1[2], bbox2[2]), max(bbox1[3], bbox2[3])]
        if bbox[0] == bbox2[0] and bbox[2] == bbox2[2]:  # whole image in the visible area
            bbox[0] = bbox1[0]
            bbox[2] = bbox1[2]
        if bbox[1] == bbox2[1] and bbox[3] == bbox2[3]:  # whole image in the visible area
            bbox[1] = bbox1[1]
            bbox[3] = bbox1[3]
        self.canvas.configure(scrollregion=bbox)  # set scroll region
        x1 = max(bbox2[0] - bbox1[0], 0)  # get coordinates (x1,y1,x2,y2) of the image tile
        y1 = max(bbox2[1] - bbox1[1], 0)
        x2 = min(bbox2[2], bbox1[2]) - bbox1[0]
        y2 = min(bbox2[3], bbox1[3]) - bbox1[1]
        if int(x2 - x1) > 0 and int(y2 - y1) > 0:  # show image if it in the visible area
            x = min(int(x2 / self.img_scale), self.width_pic)  # sometimes it is larger on 1 pixel...
            y = min(int(y2 / self.img_scale), self.height_pic)
            image = self.orgi_img.crop((int(x1 / self.img_scale), int(y1 / self.img_scale), x, y))
            self.img = ImageTk.PhotoImage(image.resize((int(x2 - x1), int(y2 - y1))))
            image_id = self.canvas.create_image(max(bbox2[0], bbox1[0]), max(bbox2[1], bbox1[1]), anchor='nw',
                                                image=self.img)
            self.canvas.lower(image_id)
            self.canvas.imagetk = self.img

    def mouse_drag(self, event):
        x = event.x
        y = event.y
        if not self.img_scale == 1.0:
            x = self.canvas.canvasx(x)
            y = self.canvas.canvasy(y)
        start = [self.start_point.x_pos * pixel_to_meter, self.height_pic - self.start_point.y_pos * pixel_to_meter,
                 self.start_point.size_point]
        start_x = start[0]
        start_y = start[1]
        size_point = start[2]
        # start_x = self.canvas.canvasx(self.start_point.x_pos * pixel_to_meter)
        # start_y = self.canvas.canvasy(self.height_pic - self.start_point.y_pos * pixel_to_meter)
        # size_point = self.start_point.size_point * self.img_scale
        if start_x - size_point <= x <= start_x + size_point and start_y - size_point <= y <= start_y + size_point:
            self.start_point.pressed = True
        # if self.start_point.x_pos * pixel_to_meter - self.start_point.size_point <= x <= \
        #         self.start_point.x_pos * pixel_to_meter + self.start_point.size_point \
        #         and self.height_pic - self.start_point.y_pos * pixel_to_meter - self.start_point.size_point <= \
        #         y <= self.height_pic - self.start_point.y_pos * pixel_to_meter + self.start_point.size_point:
        #     self.start_point.pressed = True

        elif self.end_point.x_pos * pixel_to_meter - self.end_point.size_point <= x <= \
                self.end_point.x_pos * pixel_to_meter + self.end_point.size_point \
                and self.height_pic - self.end_point.y_pos * pixel_to_meter - self.end_point.size_point <= y <= \
                self.height_pic - self.end_point.y_pos * pixel_to_meter + self.end_point.size_point:
            self.end_point.pressed = True

        for waypoint in self.waypoints:
            if waypoint.x_pos * pixel_to_meter - waypoint.size_point <= x <= \
                    waypoint.x_pos * pixel_to_meter + waypoint.size_point \
                    and self.height_pic - waypoint.y_pos * pixel_to_meter - waypoint.size_point <= y <= \
                    self.height_pic - waypoint.y_pos * pixel_to_meter + waypoint.size_point:
                waypoint.pressed = True

        self.move_points(event)

    def move_points(self, event):
        if self.start_point.pressed:
            self.start_point.change_pos(x=event.x, y=event.y, heading=self.start_point.heading_pos)
            return
        if self.end_point.pressed:
            self.end_point.change_pos(x=event.x, y=event.y, heading=self.end_point.heading_pos)
            return

        for waypoint in self.waypoints:
            if waypoint.pressed:
                waypoint.change_pos(x=event.x, y=event.y, heading=waypoint.heading_pos)
                return

    def mouse_release(self, event):
        self.start_point.pressed = False
        self.end_point.pressed = False

        for waypoint in self.waypoints:
            waypoint.pressed = False

    def add(self):
        self.end_point.y = self.place_y + 50 * screen_height * (len(self.waypoints) + 3)
        self.end_point.place_all()
        self.waypoints.append(
            Point(root=self.root, canvas=self.canvas, name='waypoint{}'.format(len(self.waypoints) + 1), x=self.place_x,
                  y=self.place_y + 50 * screen_height * (len(self.waypoints) + 2),
                  color=self.colors[len(self.waypoints)], width_pic=self.width_pic, height_pic=self.height_pic))
        self.waypoints[-1].change_pos(x=8 * pixel_to_meter, y=self.height_pic - 4 * pixel_to_meter, heading=0)

    def remove(self):
        self.waypoints[-1].destroy()
        print(len(self.waypoints))
        self.waypoints.remove(self.waypoints[-1])
        print(len(self.waypoints))
        self.end_point.y = self.place_y + 50 * screen_height * (len(self.waypoints) + 2)
        self.end_point.place_all()

    def draw_scales(self):
        messeges_x = []
        messeges_y = []
        for i in range(1, 17, 1):
            messeges_x.append(Message(master=self.root, text=i))
            messeges_x[i - 1].place(x=int(i * pixel_to_meter + 12), y=self.height_pic + 10)
            self.canvas.create_line(int(i * pixel_to_meter + 12), self.height_pic + 10, i * pixel_to_meter + 12, 0,
                                    fill="yellow")
        for i in range(1, 9, 1):
            messeges_y.append(Message(master=self.root, text=i))
            messeges_y[i - 1].place(x=0, y=self.height_pic - i * pixel_to_meter)
            self.canvas.create_line(0, self.height_pic - i * pixel_to_meter, self.width_pic,
                                    self.height_pic - i * pixel_to_meter, fill="yellow")

    def draw_curve(self):
        x, y, heading, v, theta, t, w, heading_waypoints = self.optimizer.calulate()
        if self.trajectory_lists is None:
            self.trajectory_lists = []
            self.trajectory_lists.append(x)
            self.trajectory_lists.append(y)
            self.trajectory_lists.append(heading)
            self.trajectory_lists.append(v)
            self.trajectory_lists.append(theta)
            self.trajectory_lists.append(t)
            self.trajectory_lists.append(w)
            self.trajectory_lists.append(heading_waypoints)
        else:
            self.trajectory_lists[0] = x
            self.trajectory_lists[1] = y
            self.trajectory_lists[2] = heading
            self.trajectory_lists[3] = v
            self.trajectory_lists[4] = theta
            self.trajectory_lists[5] = t
            self.trajectory_lists[6] = w
            self.trajectory_lists[7] = heading_waypoints

        for index in range(len(self.trajectory_lists[7])):
            if not self.waypoints[index].state_heading():
                self.waypoints[index].change_pos(self.waypoints[index].x_pos * pixel_to_meter,
                                                 self.height_pic - self.waypoints[index].y_pos * pixel_to_meter,
                                                 math.degrees(self.trajectory_lists[7][index]))

        for index in range(len(self.ovals)):
            self.canvas.delete(self.ovals[index])
        for index in range(len(self.trajectory_lists[0])):
            x_oval = (self.trajectory_lists[0][index] + self.start_point.x_pos) * pixel_to_meter
            y_oval = self.height_pic - (self.trajectory_lists[1][index] + self.start_point.y_pos) * pixel_to_meter
            self.ovals.append(
                self.canvas.create_oval(x_oval - self.size_point, y_oval - self.size_point, x_oval + self.size_point,
                                        y_oval + self.size_point, outline='white', fill=''))

        self.start_point.draw_point()
        self.end_point.draw_point()
        for waypoint in self.waypoints:
            waypoint.draw_point()
            print(waypoint())

    def calculate(self):
        end_u = [False, DM([0, 0, 0])]
        x_init = DM([0, 0, math.radians(self.start_point.heading_pos)])
        x_target = DM([self.end_point.x_pos - self.start_point.x_pos, self.end_point.y_pos - self.start_point.y_pos,
                       math.radians(self.end_point.heading_pos)])
        waypoints = []
        print(self.waypoints)
        for waypoint in self.waypoints:
            waypoint.update_vels()
            waypoints.append([DM([waypoint.x_pos - self.start_point.x_pos, waypoint.y_pos - self.start_point.y_pos,
                                  math.radians(waypoint.heading_pos)]), waypoint.state_heading(), waypoint.vel,
                              waypoint.theta, waypoint.rotation])
        self.optimizer = SwerveOptimize(max_v=self.max_v, max_a=self.max_a, max_theta_dot=self.max_theta_dot,
                                        max_omega=self.max_omega, max_omega_dot=self.max_omega_dot, x_init=x_init,
                                        x_target=x_target, u_target=end_u, waypoints=waypoints)
        self.draw_curve()

    def save(self):
        self.save_enable = True
        self.filename = self.save_text.get("1.0", 'end-1c')
        start_dir = os.getcwd()
        os.chdir(start_dir + '\\curves')
        if not os.path.exists('./' + self.filename + '/'):
            os.makedirs('./' + self.filename + '/')
        os.chdir('./' + self.filename + '/')
        with open(r'{}.pkl'.format(self.filename), 'wb') as f:
            list_points = [[self.filename, self.max_v, self.max_a],
                           [self.start_point.x_pos, self.start_point.y_pos, self.start_point.heading_pos],
                           [self.end_point.x_pos, self.end_point.y_pos, self.end_point.heading_pos]]
            list_points[0].append(self.max_theta_dot)
            list_points[0].append(self.max_omega)
            list_points[0].append(self.max_omega_dot)
            print('save')
            for index in range(len(self.waypoints)):
                print(self.waypoints[index]())
                list_points.append(self.waypoints[index]())
            pickle.dump(list_points, f)
            f.close()
        im = ImageGrab.grab()
        im.save(r'{}-field.jpg'.format(self.filename))
        self.trajectory()
        os.chdir(start_dir)
        self.save_enable = False

    def load(self):
        for index in range(len(self.waypoints)):
            self.waypoints[index].destroy()
        with open(r'curves\{}\{}.pkl'.format(self.filename, self.filename), 'rb') as input_file:
            list_points = pickle.load(input_file)
            print(list_points)
            for waypoint in range(len(self.waypoints)):
                self.remove()
            self.waypoints = []
            for index in range(len(list_points)):
                if index == 0:
                    self.save_text.delete('1.0', END)
                    self.save_text.insert(END, list_points[index][0])
                    self.max_v_text.delete('1.0', END)
                    self.max_v_text.insert(END, list_points[index][1])
                    self.set_max('max v', False)
                    self.max_a_text.delete('1.0', END)
                    self.max_a_text.insert(END, list_points[index][2])
                    self.set_max('max a', False)
                    time.sleep(0.01)
                    self.max_theta_dot_text.delete('1.0', END)
                    self.max_theta_dot_text.insert(END, math.degrees(list_points[index][3]))
                    self.set_max('max theta dot', False)
                    self.max_omega_text.delete('1.0', END)
                    self.max_omega_text.insert(END, math.degrees(list_points[index][4]))
                    self.set_max('max omega', False)
                    self.max_omega_dot_text.delete('1.0', END)
                    self.max_omega_dot_text.insert(END, list_points[index][5])
                    self.set_max('max omega dot', False)
                elif index == 1:
                    self.start_point.x_text.delete('1.0', END)
                    self.start_point.y_text.delete('1.0', END)
                    self.start_point.heading_text.delete('1.0', END)
                    self.start_point.x_text.insert(END, list_points[index][0])
                    self.start_point.y_text.insert(END, list_points[index][1])
                    self.start_point.heading_text.insert(END, list_points[index][2])
                    self.start_point.inputs(False)
                    print(self.start_point.x_pos, self.start_point.y_pos, self.start_point.heading_pos)
                elif index == 2:
                    self.end_point.x_text.delete('1.0', END)
                    self.end_point.y_text.delete('1.0', END)
                    self.end_point.heading_text.delete('1.0', END)
                    self.end_point.x_text.insert(END, list_points[index][0])
                    self.end_point.y_text.insert(END, list_points[index][1])
                    self.end_point.heading_text.insert(END, list_points[index][2])
                    self.end_point.inputs(False)
                    print(self.end_point.x_pos, self.end_point.y_pos, self.end_point.heading_pos)
                else:
                    self.add()
                    self.waypoints[-1].x_text.delete('1.0', END)
                    self.waypoints[-1].y_text.delete('1.0', END)
                    self.waypoints[-1].heading_text.delete('1.0', END)
                    self.waypoints[-1].v_text.delete('1.0', END)
                    self.waypoints[-1].theta_text.delete('1.0', END)
                    self.waypoints[-1].rotation_text.delete('1.0', END)
                    self.waypoints[-1].x_text.insert(END, list_points[index][0])
                    self.waypoints[-1].y_text.insert(END, list_points[index][1])
                    self.waypoints[-1].heading_text.insert(END, list_points[index][2])
                    # self.waypoints[-1].set_state_heading(list_points[index][3])
                    if list_points[index][3] == False:
                        pass
                    else:
                        self.waypoints[-1].v_text.insert(END, list_points[index][3])
                    if list_points[index][4] == False:
                        pass
                    else:
                        self.waypoints[-1].theta_text.insert(END, math.degrees(list_points[index][4]))
                    if list_points[index][5] == False:
                        pass
                    else:
                        self.waypoints[-1].rotation_text.insert(END, list_points[index][5])
                    self.waypoints[-1].inputs(False)
            input_file.close()
        self.calculate()

    def run_load_thread(self):
        if self.load_var.get() == 'load':
            pass
        elif self.load_var.get() == self.last_load_curve:
            pass
        else:
            print(self.load_var.get())
            self.filename = self.load_var.get()
            self.load()
            self.last_load_curve = self.load_var.get()
        if self.finish:
            return

    def trajectory(self):
        # self.optimizer.main()
        x = self.trajectory_lists[0]
        y = self.trajectory_lists[1]
        heading = self.trajectory_lists[2]
        t = None
        # Swerve values
        v = None
        theta = None
        w = None
        v = self.trajectory_lists[3]
        theta = self.trajectory_lists[4]
        t = self.trajectory_lists[5]
        w = self.trajectory_lists[6]

        waypoints_x = [-self.start_point.x_pos + i.x_pos for i in self.waypoints]
        waypoints_y = [-self.start_point.y_pos + i.y_pos for i in self.waypoints]

        waypoints_x = [0] + waypoints_x + [self.end_point.x_pos - self.start_point.x_pos]
        waypoints_y = [0] + waypoints_y + [self.end_point.y_pos - self.start_point.y_pos]

        # plot the trajectory path
        plt.cla()
        plt.subplot(221)
        # plt.plot(sol_X[:, 0], sol_X[:, 1])
        plt.plot(x, y)
        plt.plot(waypoints_x, waypoints_y, 'ro', ms=3)
        plt.title("X-Y")
        plt.axis("equal")

        # plot the angle
        plt.subplot(222)
        # plt.plot(sol.value(X[2, :]))
        new_heading = []
        for i in range(len(heading)):
            new_heading.append(math.degrees(heading[i]))
        plt.plot(t, new_heading)
        plt.title("heading")

        # plot V
        plt.subplot(223)
        # plt.plot(sol_U[:, 0])
        # plt.plot(t, v)
        plt.plot(t, v)
        plt.title("v")

        # plot theta and omega
        plt.subplot(224)
        # plt.plot(sol_U[:, 1])
        # plt.plot(sol_U[:, 2])
        new_theta = []
        new_w = []
        for i in range(len(theta)):
            new_theta.append(math.degrees(theta[i]))
            new_w.append(math.degrees(w[i]))
        plt.plot(t, new_theta)
        plt.plot(t, new_w)
        plt.title("theta_dot and omega")

        if self.save_enable:
            plt.savefig(r'{}-traj.jpg'.format(self.filename))
        else:
            plt.show()


if __name__ == '__main__':
    root = Tk()
    root.attributes('-fullscreen', True)
    root.title('Path Finder')
    # root.configure(background='darkorchid')
    root.configure(background='darkviolet')
    # root.configure(background='purple')

    screen_width = root.winfo_screenwidth() / 1920.0
    screen_height = root.winfo_screenheight() / 1080.0

    # pixel_to_meter = screen_height * height_picture / 8.23
    pixel_to_meter = screen_height * height_picture / 8.21
    orgi_pixel_to_meter = pixel_to_meter

    print(screen_width * 1920, screen_height * 1080)

    # path = 'field2019.png'
    path = '2020field.png'
    image = Image.open(path)
    image = image.resize((int(width_picture * screen_width), int(height_picture * screen_height)), Image.ANTIALIAS)
    img = ImageTk.PhotoImage(image)

    gui = GUI(root, path, int(width_picture * screen_width), int(height_picture * screen_height))

    root.mainloop()
    gui.finish = True
