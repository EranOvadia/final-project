from final_project.client import *
from final_project.gui import *

# width_picture = 1393
width_picture = 1377
height_picture = 700


class GUIClient(GUI, Client):
    def __init__(self, root, path_pic, width_pic, height_pic, ip='127.0.0.1', port=8100):
        GUI.__init__(self, root, path_pic, width_pic, height_pic)
        Client.__init__(self, ip=ip, port=port)
        self.load_curves = self._recv_list_load_curves()
        self.load_option_menu.destroy()
        self.load_option_menu = OptionMenu(self.root, self.load_var, *self.load_curves)
        self.load_option_menu.place(x=self.width_pic + 100 * screen_width, y=300 * screen_height)

    def _convert_DM_to_list(self, point):
        heading = point[2]
        print(heading)
        if heading is '':
            heading = 0
        consider_heading = point[2]
        if not consider_heading is '':
            consider_heading = True
        else:
            consider_heading = False
        return [[point[0], point[1], heading], consider_heading, point[3], point[4], point[5]]

    # def _minus_x_y(self, point, start_point):
    #     return [point[0] - start_point[0], point[1] - start_point[1], point[2]]

    def _update_points(self):
        self._start_point = self._convert_DM_to_list(self.start_point())[0]
        self._end_point = self._convert_DM_to_list(self.end_point())[0]
        self._waypoints = []
        for waypoint in self.waypoints:
            waypoint.update_vels()
            self._waypoints.append(self._convert_DM_to_list(waypoint()))
            print('waypoint ', self._waypoints[-1][1])

        # self._start_point = self._minus_x_y(start_point, start_point)
        # self._end_point = self._minus_x_y(end_point, start_point)
        # self._waypoints = []
        # for waypoint in waypoints:
        #     self._waypoints.append([self._minus_x_y(waypoint[0], start_point), waypoint[1]])

    def _update_maxes(self):
        self.maxes = [self.max_v, self.max_a, self.max_theta_dot, self.max_omega, self.max_omega_dot]

    def _recv_points(self):
        # full_msg = b''
        # new_msg = True
        # while True:
        #     data = self.socket.recv(16)
        #     if new_msg:
        #         msg_len = int(data[:self.HEADERSIZE])
        #         new_msg = False
        #
        #     # print(f'full message length: {msg_len}')
        #
        #     full_msg += data
        #
        #     # print(len(full_msg))
        #
        #     if len(full_msg) - self.HEADERSIZE == msg_len:
        #         # print('full msg recved')
        #         data_recv = pickle.loads(full_msg[self.HEADERSIZE:])
        #         break
        # points = data_recv
        points = self._recv_and_decompress()
        for waypoint in self.waypoints:
            self.remove()
        self.waypoints = []
        for index in range(len(points[0])):
            if points[0][index][0] == 'max_v':
                self.max_v_text.delete('1.0', END)
                self.max_v_text.insert(END, points[0][index][1])
                self.set_max('max v', False)
            elif points[0][index][0] == 'max_a':
                self.max_a_text.delete('1.0', END)
                self.max_a_text.insert(END, points[0][index][1])
                self.set_max('max a', False)
            elif points[0][index][0] == 'max_theta_dot':
                self.max_theta_dot_text.delete('1.0', END)
                self.max_theta_dot_text.insert(END, math.degrees(points[0][index][1]))
                self.set_max('max theta dot', False)
            elif points[0][index][0] == 'max_omega':
                self.max_omega_text.delete('1.0', END)
                self.max_omega_text.insert(END, math.degrees(points[0][index][1]))
                self.set_max('max omega', False)
            elif points[0][index][0] == 'max_omega_dot':
                self.max_omega_dot_text.delete('1.0', END)
                self.max_omega_dot_text.insert(END, math.degrees(points[0][index][1]))
                self.set_max('max omega dot', False)
        for index in range(len(points[1])):
            if index == 0:
                self.start_point.x_text.delete('1.0', END)
                self.start_point.y_text.delete('1.0', END)
                self.start_point.heading_text.delete('1.0', END)
                self.start_point.x_text.insert(END, points[1][index][1])
                self.start_point.y_text.insert(END, points[1][index][2])
                self.start_point.heading_text.insert(END, points[1][index][3])
                self.start_point.inputs(False)
            elif index == 1:
                self.end_point.x_text.delete('1.0', END)
                self.end_point.y_text.delete('1.0', END)
                self.end_point.heading_text.delete('1.0', END)
                self.end_point.x_text.insert(END, points[1][index][1])
                self.end_point.y_text.insert(END, points[1][index][2])
                self.end_point.heading_text.insert(END, points[1][index][3])
                self.end_point.inputs(False)
            else:
                self.add()
                self.waypoints[-1].x_text.delete('1.0', END)
                self.waypoints[-1].y_text.delete('1.0', END)
                self.waypoints[-1].heading_text.delete('1.0', END)
                self.waypoints[-1].v_text.delete('1.0', END)
                self.waypoints[-1].theta_text.delete('1.0', END)
                self.waypoints[-1].rotation_text.delete('1.0', END)
                self.waypoints[-1].x_text.insert(END, points[1][index][1])
                self.waypoints[-1].y_text.insert(END, points[1][index][2])
                if points[1][index][4] is True:
                    self.waypoints[-1].heading_text.insert(END, points[1][index][3])
                if not points[1][index][5] == 'None':
                    self.waypoints[-1].v_text.insert(END, points[1][index][5])
                else:
                    self.waypoints[-1].vel = False
                if not points[1][index][6] == 'None':
                    self.waypoints[-1].theta_text.insert(END, points[1][index][6])
                else:
                    self.waypoints[-1].theta = False
                if not points[1][index][7] == 'None':
                    self.waypoints[-1].rotation_text.insert(END, points[1][index][7])
                    # print(self.waypoints[-1].rotation_text.get('1.0', 'end-1c'))
                else:
                    self.waypoints[-1].rotation = False
                self.waypoints[-1].inputs(False)
        self.save_text.delete('1.0', END)
        self.save_text.insert(END, self.filename)
        self.calculate()

    def _points(self, debug=False):
        print('start _points')
        self._update_points()
        self._send_points(debug)
        self._update_maxes()
        time.sleep(0.1)
        self._send_maxes()
        is_trajectory = self._recv_trajectory()
        if is_trajectory:
            print(f'trajectory is ok')
        else:
            print(f'something went wrong with trajectory')
        print('finish _points')
        return is_trajectory

    def calculate(self):
        print('start calculate')
        time.sleep(1)
        is_points = self._points()
        if is_points:
            self.trajectory_lists = self.trajectory
            self.draw_curve()
            # self.root.config(cursor='')
            data = self.socket.recv(1024).decode('utf-8')
            root = Tk()
            msg = Message(master=root, text=data, width=100)
            msg.place(x=100, y=100)
            root.mainloop()
        else:
            print(f'something went wrong with points')
            root = Tk()
            msg = Message(master=root, text='calculate failed', width=100)
            msg.place(x=100, y=100)
            root.mainloop()
        print('finish calculate')

    def draw_curve(self):
        for index in range(len(self.ovals)):
            self.canvas.delete(self.ovals[index])
        for index in range(len(self.trajectory_lists[0])):
            x_oval = (self.trajectory_lists[0][index] + self.start_point.x_pos) * pixel_to_meter
            y_oval = self.height_pic - (self.trajectory_lists[1][index] + self.start_point.y_pos) * pixel_to_meter
            self.ovals.append(
                self.canvas.create_oval(x_oval - self.size_point, y_oval - self.size_point, x_oval + self.size_point,
                                        y_oval + self.size_point, outline='white', fill=''))
        for index in range(len(self.trajectory_lists[7])):
            print(math.degrees(self.trajectory_lists[7][index]))
            if not self.waypoints[index].state_heading():
                self.waypoints[index].change_pos(self.waypoints[index].x_pos * pixel_to_meter,
                                                 self.height_pic - self.waypoints[index].y_pos * pixel_to_meter,
                                                 math.degrees(self.trajectory_lists[7][index]))

    def save(self):
        self.filename = self.save_text.get("1.0", 'end-1c')
        self._save(self.filename, False)
        data = self.socket.recv(1024).decode('utf-8')
        root = Tk()
        msg = Message(master=root, text=data, width=50)
        msg.place(x=100, y=100)
        root.mainloop()

    def load(self):
        self._load(self.filename, False)

    @staticmethod
    def main(path=None, ip='127.0.0.1', port=8100):
        root = Tk()
        root.attributes('-fullscreen', True)
        root.title('Cool Path Finder')
        root.tk.call('wm', 'iconphoto', root._w, tk.PhotoImage(file='Logo.png'))
        root.configure(background='darkviolet')
        screen_width = root.winfo_screenwidth() / 1920.0
        screen_height = root.winfo_screenheight() / 1080.0

        # pixel_to_meter = screen_height * height_picture / 8.23
        pixel_to_meter = screen_height * height_picture / 8.21
        orgi_pixel_to_meter = pixel_to_meter

        print(screen_width * 1920, screen_height * 1080)

        # path = 'field2019.PNG'
        # image = Image.open(path)
        # image = image.resize((int(width_picture * screen_width), int(height_picture * screen_height)), Image.ANTIALIAS)
        # img = ImageTk.PhotoImage(image)

        with open(r'setting', 'r') as f:
            ip = f.readline()
            ip = ip[3:-1]
            port = f.readline()
            port = port[5:]
            port = int(port)
            f.close()

        g = GUIClient(root, path, int(width_picture * screen_width), int(height_picture * screen_height), ip=ip,
                      port=port)

        root.mainloop()


if __name__ == '__main__':
    GUIClient.main(path='2020field.PNG')
