import pickle
import socket
import zlib

import matplotlib.pyplot as plt


class Client(object):
    ip = None
    port = None

    HEADERSIZE = 10

    _is_finish = False

    socket = None

    commands = ['points', 'save', 'load', 'quit']

    trajectory = None

    maxes = None

    _start_point = None
    _end_point = None
    _waypoints = None

    def __init__(self, ip='127.0.0.1', port=8100):
        self.ip = ip
        self.port = port

        self._init_client()
        if type(self) == Client:
            self._recv_list_load_curves()

    def _init_client(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((self.ip, self.port))
        print(f'connect to ip: {self.ip} on port: {self.port}')

    def _compress_and_send(self, pkl):
        data = zlib.compress(pkl, 6)
        self.socket.send(str(len(data)).encode('utf-8'))
        self.socket.send(data)

    def _recv_and_decompress(self):
        len_msg = int(self.socket.recv(1024).decode('utf-8'))
        data = self.socket.recv(len_msg)
        data = zlib.decompress(data)
        return pickle.loads(data)

    def _recv_list_load_curves(self):
        data = self.socket.recv(1024).decode('utf-8')
        data = data[1:-1]
        load_curves = []
        for curve in data.split(', '):
            load_curves.append(curve[1:-1])
        return load_curves

    def _choose_command(self):
        inctrctions = 'Enter a command from the commands below:\n'
        for command in self.commands:
            inctrctions += f'-{command}\n'
        command = input(inctrctions)
        if command == 'points':
            self._points()
        elif command == 'save':
            self._save()
        elif command == 'load':
            self._load()
        elif command == 'quit':
            self._quit()
        elif command == 'debug':
            self._debug()
        else:
            print(f'the command \'{command}\' isn\'t exists')

    def _quit(self):
        self.socket.send('quit'.encode('utf-8'))
        self.socket.close()
        print(f'connection with {(self.ip, self.port)} was close')
        self._is_finish = True

    def _update_points(self):
        raise NotImplemented

    def _update_maxes(self):
        raise NotImplemented

    def _send_maxes(self):
        pkl = pickle.dumps(self.maxes)
        # msg = bytes(f'{len(pkl):<{self.HEADERSIZE}}', 'utf-8') + pkl
        # self.socket.send(msg)
        self._compress_and_send(pkl)

    def _send_points(self, debug):
        if not debug:
            self.socket.send('points'.encode('utf-8'))
        # start_point = [0, 0, 0]
        # end_point = [4, 0, 0]
        # waypoints = [[[2, 2, 0], False]]
        #
        # end_point[0] = end_point[0] - start_point[0]
        # end_point[1] = end_point[1] - start_point[1]
        #
        # for waypoint in waypoints:
        #     waypoint[0][0] = waypoint[0][0] - start_point[0]
        #     waypoint[0][1] = waypoint[0][1] - start_point[1]
        #
        # start_point = [0, 0, 0]
        #
        # self._start_point = start_point
        # self._end_point = end_point
        # self._waypoints = waypoints

        final_list = [self._start_point, self._end_point, self._waypoints]
        pkl = pickle.dumps(final_list)
        # msg = bytes(f'{len(pkl):<{self.HEADERSIZE}}', 'utf-8') + pkl
        # self.socket.send(msg)
        self._compress_and_send(pkl)

    def _recv_points(self):
        raise NotImplemented

    def _recv_trajectory(self):
        # full_msg = b''
        # new_msg = True
        # while True:
        #     data = self.socket.recv(16)
        #     if new_msg:
        #         msg_len = int(data[:self.HEADERSIZE])
        #         new_msg = False
        #
        #     # print(f'full message length: {msg_len}')
        #
        #     full_msg += data
        #
        #     # print(len(full_msg))
        #
        #     if len(full_msg) - self.HEADERSIZE == msg_len:
        #         # print('full msg recved')
        #         data_recv = pickle.loads(full_msg[self.HEADERSIZE:])
        #         break
        # self.trajectory = data_recv
        data = self.socket.recv(1024).decode('utf-8')
        if data == 'succesed calculating':
            self.trajectory = self._recv_and_decompress()
            return True
        elif data == 'failed calculating':
            return False

    def _show_trajectory(self):
        waypoints_x = [i[0][0] for i in self._waypoints]
        waypoints_y = [i[0][1] for i in self._waypoints]

        waypoints_x = [self._start_point[0]] + waypoints_x + [self._end_point[0]]
        waypoints_y = [self._start_point[1]] + waypoints_y + [self._end_point[1]]

        plt.subplot(221)
        plt.plot(self.trajectory[0], self.trajectory[1])
        plt.plot(waypoints_x, waypoints_y, 'ro', ms=3)
        plt.axis('equal')

        plt.subplot(222)
        plt.plot(self.trajectory[5], self.trajectory[2])
        plt.axis('equal')

        plt.subplot(223)
        plt.plot(self.trajectory[5], self.trajectory[3])

        plt.subplot(224)
        plt.plot(self.trajectory[5], self.trajectory[4])
        plt.plot(self.trajectory[5], self.trajectory[6])

        plt.show()

    def _points(self, debug=False):
        self._send_points(debug)
        self._send_maxes()
        is_trajectory = self._recv_trajectory()
        if is_trajectory:
            self._show_trajectory()
        else:
            print(f'something went wrong with trajectory')

    def _save(self, filename, debug):
        if not debug:
            self.socket.send('save'.encode('utf-8'))
        self.socket.send(f'{filename}'.encode('utf-8'))

    def _show_curve_from_load(self):
        if type(self) == Client:
            self._recv_trajectory()
            self._show_trajectory()
        else:
            self._recv_points()
            # self._recv_trajectory()

    def _load(self, filename, debug):
        if not debug:
            self.socket.send('load'.encode('utf-8'))
        self.socket.send(f'{filename}'.encode('utf-8'))
        self._show_curve_from_load()

    def _debug(self):
        self.socket.send('debug'.encode('utf-8'))
        print('you need a password for this command')
        is_password = False
        while not is_password:
            password = input()
            self.socket.send(password.encode('utf-8'))
            data = self.socket.recv(1024).decode('utf-8')
            if data == 'True':
                is_password = True
            else:
                print('sorry wrong password')

        self._points(True)
        # self._save(True)
        # self._load(True)

    def run(self, gui=False):
        if not gui:
            while not self._is_finish:
                self._choose_command()
        return


if __name__ == '__main__':
    c = Client()
    c.run()
