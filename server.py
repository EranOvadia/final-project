import math
import pickle
import select
import socket
import threading
import zlib

from final_project.swerve_optimize import *
from database import *


class ServerClient(object):
    HEADERSIZE = 10

    def __init__(self, socket, address):
        self.socket = socket
        self.address = address

        self.is_close = False

        self.trajectory_list = None

        self.points = None
        self.maxes = None  # ['Swerve', max_v, max_a, max_theta_dot, max_omega, max_omega_dot, [False, [0, 0, 0]]]
        self.command = None

        self.optimzer = None

        self.filename = None
        self.db = None

        self._send_list_load_curves()

    def _compress_and_send(self, pkl):
        data = zlib.compress(pkl, 6)
        self.socket.send(str(len(data)).encode('utf-8'))
        self.socket.send(data)

    def _recv_and_decompress(self):
        len_msg = int(self.socket.recv(1024).decode('utf-8'))
        data = self.socket.recv(len_msg)
        data = zlib.decompress(data)
        return pickle.loads(data)

    def _send_list_load_curves(self):
        start_dir = os.getcwd()
        os.chdir(start_dir + '\\curves')
        load_curves = ['load']
        for name in os.listdir('.'):
            load_curves.append(name[:-3])
        os.chdir(start_dir)
        self.socket.send(f'{load_curves}'.encode('utf-8'))

    def _recv_command(self):
        try:
            self.command = self.socket.recv(1024).decode('utf-8')
        except Exception as e:
            print(e)
            self.command = 'quit'

        if self.command == 'points':
            self._points()
        if self.command == 'save':
            self._save()
        if self.command == 'load':
            self._load()
        if self.command == 'quit':
            self._quit()
        if self.command == 'debug':
            self._debug()

    def _quit(self):
        self.socket.close()
        self.is_close = True

    def _recv_maxes(self):
        print('start _recv_maxes')
        # full_msg = b''
        # new_msg = True
        # while True:
        #     data = self.socket.recv(16)
        #     if new_msg:
        #         # print('new msg len: ', data[:self.HEADERSIZE])
        #         msg_len = int(data[:self.HEADERSIZE])
        #         new_msg = False
        #
        #     # print(f'full message length: {msg_len}')
        #
        #     full_msg += data
        #
        #     print(len(full_msg))
        #
        #     if len(full_msg) - self.HEADERSIZE == msg_len:
        #         # print('full msg recved')
        #         # print(full_msg[self.HEADERSIZE:])
        #         data_recv = pickle.loads(full_msg[self.HEADERSIZE:])
        #         # print(data_recv)
        #         break
        # self.maxes = data_recv
        self.maxes = self._recv_and_decompress()
        print('finish _recv_maxes')

    def _recv_points(self):
        print('start _recv_points')
        # full_msg = b''
        # new_msg = True
        # while True:
        #     data = self.socket.recv(16)
        #     if new_msg:
        #         # print('new msg len: ', data[:self.HEADERSIZE])
        #         msg_len = int(data[:self.HEADERSIZE])
        #         new_msg = False
        #
        #     # print(f'full message length: {msg_len}')
        #
        #     full_msg += data
        #
        #     # print(len(full_msg))
        #
        #     if len(full_msg) - self.HEADERSIZE == msg_len:
        #         # print('full msg recved')
        #         # print(full_msg[self.HEADERSIZE:])
        #         data_recv = pickle.loads(full_msg[self.HEADERSIZE:])
        #         # print(data_recv)
        #         break
        # self.points = data_recv
        self.points = self._recv_and_decompress()
        self.points[0][2] = math.radians(self.points[0][2])
        self.points[1][2] = math.radians(self.points[1][2])
        for index in range(len(self.points[2])):
            self.points[2][index][0][2] = math.radians(self.points[2][index][0][2])
        print('finish _recv_points')

    def _send_trajectory(self):
        pkl = pickle.dumps(self.trajectory_list)
        # msg = bytes(f'{len(pkl):<{self.HEADERSIZE}}', 'utf-8') + pkl
        # self.socket.send(msg)
        self._compress_and_send(pkl)

    @staticmethod
    def minus_x_y(point, start_point):
        return DM([point[0] - start_point[0], point[1] - start_point[1], point[2]])

    def _start_optimzer(self):
        start_point = [self.points[0][0], self.points[0][1], self.points[0][2]]
        x_init = ServerClient.minus_x_y(self.points[0], start_point)
        x_target = ServerClient.minus_x_y(self.points[1], start_point)
        waypoints = [[[point[0][0], point[0][1], point[0][2]], point[1], point[2], point[3], point[4]] for point in
                     self.points[2]]
        for waypoint in waypoints:
            waypoint[0] = ServerClient.minus_x_y(waypoint[0], start_point)
        # def __init__(self, max_v, max_a, max_theta_dot, max_omega, max_omega_dot, x_init, x_target, u_target, waypoints):
        # self.optimzer = SwerveOptimize(3, 3, 3, 3, 3, x_init, x_target, [False, DM([0, 0, 0])], waypoints=waypoints)
        u_target = [False, DM([0, 0, 0])]
        self.optimzer = SwerveOptimize(max_v=self.maxes[0], max_a=self.maxes[1], max_theta_dot=self.maxes[2],
                                       max_omega=self.maxes[3], max_omega_dot=self.maxes[4], x_init=x_init,
                                       x_target=x_target, u_target=u_target, waypoints=waypoints)
        try:
            x, y, heading, v, theta, t, w, heading_waypoints = self.optimzer.calculate()
            print(waypoints)
            print(heading_waypoints)
        except Exception as e:
            print(e)
            self.socket.send(f'failed calculating'.encode('utf-8'))
            return False
        self.trajectory_list = [x, y, heading, v, theta, t, w, heading_waypoints]
        self.socket.send(f'succesed calculating'.encode('utf-8'))
        return True

    def _points(self):
        print('enter _points')
        self._recv_points()
        self._recv_maxes()
        is_calculated = self._start_optimzer()
        if is_calculated:
            self._send_trajectory()
            self.socket.send('finish calculating'.encode('utf-8'))
        print('finish _points')

    def _save(self):
        print('enter save')
        start_dir = os.getcwd()
        os.chdir(start_dir + '\\curves')
        self.filename = self.socket.recv(1024).decode('utf-8')
        self.db = DataBase(self.filename)
        self.db.connect()
        try:
            self.db.drop_tables_points_and_maxes()
        except Exception as e:
            print(e)
        self.db.create_table_points()
        self.db.create_table_maxes()
        value_points = [
            ['start_point', self.points[0][0], self.points[0][1], math.degrees(self.points[0][2]), False, None, None,
             None],
            ['end_point', self.points[1][0], self.points[1][1], math.degrees(self.points[1][2]), False, None, None,
             None]]
        for waypoint in range(len(self.points[2])):
            vel = self.points[2][waypoint][2]
            if vel:
                vel = vel
            else:
                vel = None
            theta = self.points[2][waypoint][3]
            if theta:
                theta = math.degrees(theta)
            else:
                theta = None
            rotation = self.points[2][waypoint][4]
            if rotation:
                rotation = math.degrees(rotation)
            else:
                rotation = None
            value_points.append(
                [f'waypoint_{waypoint + 1}', self.points[2][waypoint][0][0], self.points[2][waypoint][0][1],
                 math.degrees(self.points[2][waypoint][0][2]), self.points[2][waypoint][1], vel, theta, rotation])
        self.db.add_values_points(value_points)
        value_maxes = [['max_v', self.maxes[0]], ['max_a', self.maxes[1]], ['max_theta_dot', self.maxes[2]],
                       ['max_omega', self.maxes[3]], ['max_omega_dot', self.maxes[4]]]
        self.db.add_values_maxes(value_maxes)
        os.chdir(start_dir)
        self.db.close()
        self.socket.send('finish saving'.encode('utf-8'))
        print('finish save')

    def _load(self):
        print('enter load')
        self.filename = self.socket.recv(1024).decode('utf-8')
        start_dir = os.getcwd()
        os.chdir(start_dir + '\\curves')
        self.db = DataBase(self.filename)
        self.db.connect()
        values = [self.db.get_values_table('maxes'), self.db.get_values_table('points')]
        print(values)
        pkl = pickle.dumps(values)
        # msg = bytes(f'{len(pkl):<{self.HEADERSIZE}}', 'utf-8') + pkl
        # self.socket.send(msg)
        self._compress_and_send(pkl)
        self.db.close()
        os.chdir(start_dir)
        print('finish load')

    def _debug(self):
        password = self.socket.recv(1024).decode('utf-8')
        while not password == 'ez_debug':
            self.socket.send('False'.encode('utf-8'))
            password = self.socket.recv(1024).decode('utf-8')

        self.socket.send('True'.encode('utf-8'))

        self._points()
        # self._save()
        # self._load()

    def run(self):
        self._recv_command()


class Server(object):
    ip = None
    port = None

    socket = None

    clients = None

    def __init__(self, ip='0.0.0.0', port=8100):
        self.ip = ip
        self.port = port

        self._init_server()

        self.clients = []

    def _init_server(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.bind((self.ip, self.port))
        self.socket.listen(5)

    def _connect_single_client(self):
        client_socket, address = self.socket.accept()
        self.clients.append(ServerClient(client_socket, address))
        print(f'connect to address: {address}')

    def _run_client(self, client):
        while True:
            client.run()

            if client.is_close:
                self.clients.remove(client)
                return

    def run(self):
        while True:
            self._connect_single_client()
            thread = threading.Thread(target=self._run_client, args=(self.clients[-1],))
            thread.start()


if __name__ == '__main__':
    s = Server()
    s.run()
